/**
 * Represents a employee working for the hospital
 * Extends the person abstract to allow comparisons between employee and patient classes.
 */
public class Employee extends Person{

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
