import java.util.ArrayList;
import java.util.Objects;

/** Represents a department under a hospital.
 * Contains registries of all employees and patients.
 */
public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<Employee>();
        this.patients = new ArrayList<Patient>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /** Adds employee object to department registry
     * @param employee Employee object
     * @return true if employee if addition successful, false if employee already in registry.
     */
    public boolean addEmployee(Employee employee) {
        if (this.employees.contains(employee)) {
            return false;
        } else {
            return this.employees.add(employee);
        }

    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /** Adds patient object to department registry
     * @param patient Patient object
     * @return true if patient if addition successful, false if patient already in registry.
     */
    public boolean addPatient(Patient patient) {
        if (this.patients.contains(patient)) {
            return false;
        } else {
            return this.patients.add(patient);
        }
    }


    /** Removes a given person object from either registry.
     * Throws RemoveException should person not be registered.
     * @param person Person object
     * @throws RemoveException
     */
    public void remove(Person person) throws RemoveException {
        for (Patient patient: this.patients) {
            if (patient.equals(person)) {
                this.patients.remove(patient);
                return;
            }
        }

        for (Employee employee: this.employees) {
            if (employee.equals(person)) {
                this.employees.remove(employee);
                return;
            }
        }

        throw new RemoveException("No employees or patient named " + person.getFirstName() + ".");
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}
