import java.util.ArrayList;

/**
 * Represents a hospital with an associated list of departments.
 */

public class Hospital {
    final private String hospitalName;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<Department>();
    }

    public String getHospitalName() {
        return this.hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return this.departments;
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
