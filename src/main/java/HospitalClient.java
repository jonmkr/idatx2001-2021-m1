
public class HospitalClient {
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olav's Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Department department = hospital.getDepartments().get(0);
        Employee employee = department.getEmployees().get(0);


        try {
            department.remove(employee);
            System.out.println("Successfully removed employee from the hospital registry");
        } catch (RemoveException e) {
            System.out.println("Unable to remove employee, not in the hospital registry");
        }

        Patient patient = new Patient("Fornavn", "Etternavn", "");

        try {
            department.remove(patient);
            System.out.println("Successfully removed patient from the hospital registry");
        } catch (RemoveException e) {
            System.out.println("Unable to remove patient, not in the hospital registry");
        }

    }
}
