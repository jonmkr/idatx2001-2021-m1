/**
 * Represents a patient admitted to the hospital
 * Extends the person abstract to allow comparisons between employee and patient classes.
 */

public class Patient extends Person implements Diagnosable{
    private String diagnosis;

    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return this.diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "diagnosis='" + diagnosis + '\'' +
                '}';
    }
}
