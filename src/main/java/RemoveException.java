public class RemoveException extends Exception {
    private final long serialVersionUID = 1L;

    public RemoveException(String exception) {
        super(exception);
    }
}
