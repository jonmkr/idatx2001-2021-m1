import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class DepartmentTest {
    Department department;
    Employee employee;
    Patient patient;

    @Before
    public void setUp() {
        department = new Department("Test Department");
        employee = new Employee("Odd Even", "Primtallet", "");
        patient = new Patient("Inga", "Lykke", "");
    }

    @Test
    public void addTest() {
        department.addEmployee(employee);
        department.addPatient(patient);

        assertTrue(department.getEmployees().contains(employee));
        assertTrue(department.getPatients().contains(patient));

        assertFalse(department.addEmployee(employee));
        assertFalse(department.addPatient(patient));
    }

    @Test
    public void removeTest() {
        addTest();

        try {
            department.remove(employee);
        } catch (RemoveException e) {
            fail("Employee not found in department registry");
        }

        assertFalse(department.getEmployees().contains(employee));
        assertTrue(department.getPatients().contains(patient));

        try {
            department.remove(patient);
        } catch (RemoveException e) {
            fail("Patient not found in department registry");
        }

        assertFalse(department.getEmployees().contains(employee));
        assertFalse(department.getPatients().contains(patient));







    }

}
